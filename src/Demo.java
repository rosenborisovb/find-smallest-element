public class Demo {
    public static void main(String[] args) {
        Rectangle[] arrayRectabgls = {
                new Rectangle(6, 9),
                new Rectangle(4, 11),
                new Rectangle(7, 3)
        };

        Pomoshtnik pomoshtnik = new MinAreaRectangle();
        Rectangle minAreaRectangl = (Rectangle) Helpers.findMinElement(arrayRectabgls, pomoshtnik);
        System.out.println(minAreaRectangl.getHeight());

        Rectangle r1 = (Rectangle) Helpers.findMinElement(arrayRectabgls, new Pomoshtnik() {
            @Override
            public int compare(Object o1, Object o2) {
                Rectangle r1 = (Rectangle) o1;
                Rectangle r2 = (Rectangle) o2;

                double p1 = r1.getWidth() * 2 + r1.getHeight() * 2;
                double p2 = r2.getWidth() * 2 + r2.getHeight() * 2;

                if (p1 == p2){
                    return 0;
                }else if (p1 > p2){
                    return 1;
                }else {
                    return -1;
                }
            }
        });
        System.out.println(r1.getHeight());
    }
}
