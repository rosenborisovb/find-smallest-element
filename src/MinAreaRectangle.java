public class MinAreaRectangle implements Pomoshtnik {

    @Override
    public int compare(Object o1, Object o2){

        Rectangle r1 = (Rectangle) o1;
        Rectangle r2 = (Rectangle) o2;

        double a1 = r1.getWidth() * r1.getHeight();
        double a2 = r2.getWidth() * r2.getHeight();

        if (a1 == a2){
            return 0;
        }else if(a1 > a2){
            return 1;
        }else {
            return -1;
        }

    }

}
