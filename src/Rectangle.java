public class Rectangle {

    private double width;
    private double height;

    public void setWidth(double width){
        this.width = width;
    }

    public double getWidth(){
        return width;
    }

    public void setHeight(double height){
        this.height = height;
    }

    public double getHeight(){
        return height;
    }

    public Rectangle(double width, double height){
        setWidth(width);
        setHeight(height);
    }

}
