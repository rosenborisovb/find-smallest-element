public class Helpers {

    public static Object findMinElement(Object[] a, Pomoshtnik pomoshtnik){
        if (a == null || a.length == 0){
            return null;
        }

        Object min = a[0];
        for (int i = 1; i < a.length; i++){
            int result = pomoshtnik.compare(min, a[i]);
            if (result == 1){
                min = a[i];
            }
        }
        return min;
    }

}
